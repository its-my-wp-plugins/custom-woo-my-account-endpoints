<?php

class Wooacends {

    private $cs_account_endpoints = array(
        'post-creation'   => 'Post creation',
        'posts/edit-post' => 'Edit post',
        'posts'           => 'Posts',
	);

    public function __construct() {
        add_filter( 'the_title', array( $this, 'endpoint_title' ), 100 );
        add_action( 'init', array($this, 'init') );
        add_filter( 'woocommerce_get_query_vars', array( $this, 'get_query_vars' ), 0 );
    }

    public function endpoint_title( $title ) {
		global $wp_query;

        foreach($this->cs_account_endpoints as $key => $endpoint_name) {
            $is_endpoint = isset( $wp_query->query_vars[ $key ] );

            if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
                // New page title.
                $title = $endpoint_name;
    
                remove_filter( 'the_title', array( $this, 'endpoint_title' ) );

                break;
            }
        }

		return $title;
	}
    
    public function init() {
        $this->cs_account_endpoints = apply_filters('hvn_woo_account_endpints', $this->cs_account_endpoints);

        foreach($this->cs_account_endpoints as $key => $endpoint_name) {
            add_rewrite_endpoint($key, EP_ROOT | EP_PAGES);
            $this->add_action_to_output_content($key);
        }
    }

    public function set_page_var($endpoint_key){
        global $wp_query;
        $query_var = $wp_query->query_vars[$endpoint_key];
        $query_var_arr = explode('/', $query_var);
        $page_index = array_search('page', $query_var_arr);
        if($page_index !== false){
            $current_page_index = count($query_var_arr) >= $page_index + 1 ? $page_index + 1 : false;
            if($current_page_index !== false) {
                set_query_var( 'page', absint( $query_var_arr[$current_page_index] ) );
            }
        }
    }

    public function output_content() {
        global $wp_query;
        $endpoint_key = '';
        foreach($this->cs_account_endpoints as $key => $endpoint_name) {
            if(isset( $wp_query->query_vars[ $key ] )){
                $endpoint_key = $key;
            };
        }

        $this->set_page_var($endpoint_key);

        $array = apply_filters('hvn_'.$endpoint_key.'_template_params', array());

        if(!empty($endpoint_key)) {
            wc_get_template('myaccount/'.$endpoint_key.'.php', $array);
        }
    }

    public function add_action_to_output_content($key) {
        add_action('woocommerce_account_'.$key.'_endpoint', array($this, 'output_content'));
    }

    public function get_query_vars($vars) {
        foreach($this->cs_account_endpoints as $key => $endpoint_name) {
            $vars[ $key ] = $key;
        }
		return $vars;
    }

}

new Wooacends();

register_activation_hook( __FILE__, 'flush_rewrite_rules' );