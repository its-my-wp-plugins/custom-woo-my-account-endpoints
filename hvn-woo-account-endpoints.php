<?php
/**
 * Custom woo account endpoints
 *
 * Plugin Name: Custom woo account endpoints
 * Plugin URI:  https://no-site.org/
 * Description: Add custom woo my-account endpoints
 * Version:     1.0
 * Author:      Haven
 * Author URI:  https://no-ste.org/
 * Text Domain: wooacends
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Invalid request.' );
}

define( 'WOOACENDS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WOOACENDS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

include_once WOOACENDS_PLUGIN_DIR . 'hvn-woo-account-endpints-class.php';